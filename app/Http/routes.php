<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'api/v1'], function() {
	
	Route::post('apiLogin','ApiAuth\ApiAuthController@apiLogin');
	Route::post('apiRegister','ApiAuth\ApiAuthController@apiRegister');
	
	// Recipient / Shipment detail
	Route::match(array('GET', 'POST'),'apiGetShipments/{id}','ApiRecipient\ApiRecipientController@apiGetShipments');
	Route::post('apiSetShipment','ApiRecipient\ApiRecipientController@apiSetShipment');
	
});

Route::resource('recipients', 'Recipient\RecipientController');

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
