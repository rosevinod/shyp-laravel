<?php namespace App\Http\Controllers\Recipient;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Recipient;
use App\Http\Requests\RecipientRequest;
use Illuminate\Http\Request;
use Auth;
use Input;
use Config;


class RecipientController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Recipient Controller
	|--------------------------------------------------------------------------
	|
	| This controller will add the new shipment 
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @param  
	 * @param  
	 * @return void
	 */
	public function __construct() {
				
		$this->middleware('mrecipient');
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		
		 //$recipients = Recipient::where('user_id','=', Auth::user()->id)->get();		
		 $recipients = Recipient::authUser(Auth::user()->id)->get();		
    	 return view('recipient.index', compact('recipients'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		
		return view('recipient.add_shipment');
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RecipientRequest $request) {
		
	    if (Input::file('image')->isValid()) {
			
		    $request->file('image')->move(public_path('images'), $request->file('image')->getClientOriginalName());

		       $data = $request->except(['image']);
		       $data['image'] = Config::get('app.url').'/public/images/' . $request->file('image')->getClientOriginalName();

		    $recipient = Auth::user()->recipients()->create($data);

	   		session()->flash('flash_message', 'Your article has been created!!!');
				
	   		return redirect('recipients');
	     
	    }
	    else {
	      // sending back with error message.
	      Session::flash('error', 'uploaded file is not valid');
	      return redirect('recipients');
	    }
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Recipient $recipient) {

		// remove base path from shipment image
		$recipient->image = str_replace(base_path(),'',$recipient->image);
		
		return view('recipient.show', compact('recipient'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 *
	 * @binding : we are using RouteServiceProvider to bind the controller with Model
	 * to access the id values	
	 *
	 */
	public function edit(Recipient $recipient) {
		
		return view('recipient.edit', compact('recipient'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 *
	 * @binding : we are using RouteServiceProvider to bind the controller with Model
	 * to access the id values	
	 *
	 */
	public function update(Recipient $recipient, RecipientRequest $request)	{
		
	    if (Input::file('image')->isValid()) {
			
		    $request->file('image')->move(public_path('images'), $request->file('image')->getClientOriginalName());

		       $data = $request->except(['image']);
		       $data['image'] = Config::get('app.url').'/public/images/' . $request->file('image')->getClientOriginalName();

		    $recipient->update($data);

	   		session()->flash('flash_message', 'Your article has been updated!!!');
				
	   		return redirect('recipients');
	     
	    }
	    else {
	      // sending back with error message.
		  session()->flash('flash_message', 'Uploaded file is not valid');
	      return redirect('recipients');
	    }
		
				
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
