<?php namespace App\Http\Controllers\ApiRecipient;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Recipient;
use App\Http\Controllers\ApiController;
use App\Http\Requests\RecipientRequest;
use Config;

class ApiRecipientController extends ApiController {

	/**
	* @var user id
	*
	* @return shipment results
	*
	*/
	public function apiGetShipments($id) {
		
		$data = Recipient::where('user_id','=',$id)->get();
		
		if(count($data) > 0) {
			$response = $this->setStatusCode(200)->responseSuccess($data);
		} else {
			$response = $this->setStatusCode(500)->responseWithError();			
		}
		
		return $response;
		
	}// end apiGetShipments

	/**
	* @var user id
	*
	* @return shipment results
	*
	*/
	public function apiSetShipment(RecipientRequest $request) {
			
		//dd($request->get('recipient_name'));
		
   		// Create a new shipment in the database...
   		$recipient = Recipient::create($request->all());			
		$response = $this->setStatusCode(200)->responseSuccess($recipient);
		
		/*
	    if ($request->file('image')->isValid()) {
			
		    $request->file('image')->move(public_path('images'), $request->file('image')->getClientOriginalName());

		       $data = $request->except(['image']);
		       $data['image'] = Config::get('app.url').'/public/images/' . $request->file('image')->getClientOriginalName();

	   		// Create a new shipment in the database...
	   		$recipient = Recipient::create($data);			
			$response = $this->setStatusCode(200)->responseSuccess($recipient);
			
	    }
	    else {    
			$response = $this->getStatusCode(500)->responseWithError('Incorrect input!!!');

	    }*/
		
		return $response;
		
	}// end apiGetShipments
		
}
