<?php namespace App\Http\Controllers\ApiAuth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\Acme\Transformers\UserTransformer;
use App\User;
use App\Http\Controllers\ApiController;

class ApiAuthController extends ApiController {

	use AuthenticatesAndRegistersUsers;
	
	protected $userTransformer;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar (inside Services/Registrar)
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar, UserTransformer $userTransformer)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;
		$this->userTransformer = $userTransformer;

		//$this->middleware('guest', ['except' => 'getLogout']);
	}
	
	/**
	 * Handle a login request to the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function apiLogin(Guard $auth, Request $request)
	{
		//$this->auth = $auth;
		
		$this->validate($request, [
			'email' => 'required', 'password' => 'required',
		]);

		$credentials = $request->only('email', 'password');

		if ($this->auth->attempt($credentials, $request->has('remember')))
		{
			//dd($this->auth->user());
			$user_data = $this->auth->user()->toArray();
			
			$success = $this->userTransformer->transform($user_data);
			
			return $success;
		}

		return '{"statusCode":"500","status":"fail","errorMessage":"Invalid credentials"}';
	}	
	
	/**
	 * Handle to register a user using api calls
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function apiRegister(Request $request) {
		
		$user = User::email($request['email'])->get();
		
		$email_count = count($user->toArray());
		
		if($email_count > 0) {
			return $this->setStatusCode(500)->responseWithError('User already exists!!!');
		}
		
		$validator = $this->registrar->validator($request->all());

		if ($validator->fails())
		{
			return $this->setStatusCode(500)->responseWithError('Incorrect user input');
		}
		
		$user_data = $this->registrar->create($request->all());
		
		if($user_data) {
			
			$data = ["id" => $user_data->id , "name" => $user_data->name , "email" => $user_data->email];
			
			$success = $this->userTransformer->transform($data);
			
			return $success;
			
		}

		return '{"statusCode":"500","status":"fail","errorMessage":"Invalid input values"}';
	}	
	
}
