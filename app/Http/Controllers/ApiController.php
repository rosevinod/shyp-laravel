<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Response;

use Symfony\Component\HttpFoundation\Response as IlluminateResponse;

class ApiController extends Controller {

	protected $statusCode = 200;
	
	
	/**
	 * @return mixed
	 */
	
	public function getStatusCode() {
		
		return $this->statusCode ;
		
	}
	
	
	/**
	 * @return mixed
	 */
	
	public function setStatusCode($statusCode) {
		
		$this->statusCode = $statusCode;
		
		return $this;
	}
	
	/**
	 * @return mixed
	 */
	
	public function responseNotFound($message='Not Found!!') {
		
		return $this->setStatusCode(IlluminateResponse::HTTP_NOT_FOUND)->responseWithError($message);
		
	}
	
	/**
	 * @return mixed
	 */
	
	public function responseWithError($message='Result not found!!') {
				
		return $this->respond([				
				'errorMessage' => $message
				,'statusCode' => $this->getStatusCode()
				,'status' => 'fail'							
		]);
		
	}
	
	/**
	 * @return mixed
	 */
	
	public function respond($data, $headers = []) {
		
		return Response::json($data, $this->getStatusCode(), $headers);
		
	}
	
	/**
	 * @return mixed
	 */
	
	public function responseSuccess($message) {
		
		return $this->respond([			
				'responseData' => $message
				,'statusCode' => $this->getStatusCode()					
		]);
		
	}
	
}// end class LessonsController
