<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

// added custom
use Illuminate\Database\Eloquent\Relations\HasMany;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password','mobile_number','zip_code','referral_code'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];
	
	// adding the query scope for the custom query report
	// check if the email already exists
	
	public function scopeEmail($query, $emailAddress) {
		
		$query->whereEmail($emailAddress);
		
	}// end scopePublished
	
	
	/**
	 * A user can have mutiple recipients
	 *
	 * @return Illuminate\Database\Eloquent\Relations\HasMany
	 */
	
	public function recipients() {
				
		return $this->hasMany('App\Recipient');
		
	}	

}
