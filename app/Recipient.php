<?php namespace App;

use Illuminate\Database\Eloquent\Model;

// added custom
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;


class Recipient extends Model {
	

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'recipients';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id','recipient_name','country'
		,'batch_code','address','more_address','city','state','zip', 'image'];
	
	// adding the query scope for the custom query report
	// show those recipient shipment has been logged in 
	
	public function scopeAuthUser($query, $user_id) {
		
		$query->where('user_id' , '=' , $user_id);
		
	}// end scopePublished
	
	
	/**
	 * A recipient has a user , we can change the name of method ....
	 *
	 * @return Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	
	public function user() {
		
		return $this->belongsTo('App\User');
		
	}// end user
	

}
