<?php namespace App\Acme\Transformers;


abstract class Transformer {
	
	/**
	 * convert the db array to custom array
	 *
	 * @param $item
	 *
 	 * @return array
	 */
	
	public function transformCollection(array $items){
		
		return array_map([$this, 'transform'], $items);		
		
	}
	
	public abstract function transform($item);
	
	
}
