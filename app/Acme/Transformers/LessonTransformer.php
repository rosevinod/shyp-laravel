<?php namespace App\Acme\Transformers;

class LessonTransformer extends Transformer {
	
	/**
	 * convert the db array to custom array
	 *
	 * @param $item
	 *
 	 * @return array
	 */
	
	public function transform($lesson){
		
		//dd($lesson);
				
		return [ 'title' => $lesson['title']
			,'body' => $lesson['body']
			,'active' => $lesson['some_bool']
		];
		
	}
	
}	
	
