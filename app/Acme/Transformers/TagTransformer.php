<?php namespace App\Acme\Transformers;

class TagTransformer extends Transformer {
	
	/**
	 * convert the db array to custom array
	 *
	 * @param $item
	 *
 	 * @return array
	 */
	
	public function transform($tag){
		
		//dd($tag);
				
		return [ 'name' => $tag['name']	];
		
	}
	
}	
	
