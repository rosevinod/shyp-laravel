<?php namespace App\Acme\Transformers;

class UserTransformer extends Transformer {
	
	/**
	 * convert the db array to custom array
	 *
	 * @param $item
	 *
 	 * @return array
	 */
	
	public function transform($user_data){
		
		//dd($lesson);
				
		return array("statusCode" => (int) 200, "status" => "success" 
				,"responseData" => $user_data);
		
	}
	
}	
	
