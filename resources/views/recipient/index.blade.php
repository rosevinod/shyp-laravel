@extends('app')

@section('content')

	<h1>List of Shipment</h1> 
	<h3><a href="{{ url('/recipients/create') }}">Add Shipment</a></h3>

	<table class="table">
	    <thead>
	        <tr>
	            <th>ID</th>
	            <th>Name</th>
	            <th>Country</th>
				<th>Address</th>
	        </tr>
			
			@if(count($recipients) > 0)
			
			@foreach($recipients as $recipient)
			
		        <tr>
		            <td><a href="{{ action('Recipient\RecipientController@edit', $recipient->id) }}">Edit</a></td>
					<td><a href="{{ url('/recipients' , $recipient->id) }}" >{{ $recipient->recipient_name }}</td>
		            <td>{{ $recipient->country }}</td>
					<td>{{ $recipient->address }}</td>
		        </tr>
			
			@endforeach
			
			@else
		        <tr>		            
					<td colspan="4">You havn't added shippment yet!!!</td>
		        </tr>
			@endif
			
			
	    </thead>
	</table>

@stop