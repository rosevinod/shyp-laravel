@extends('app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
						
						{!! HTML::image($recipient->image, 'a picture'
						, array('class' => 'img-rounded img-responsive')) !!}
					   
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <h4>
							{{$recipient->recipient_name}}</h4>
                        <small><cite title="{{ $recipient->address }}">{{ $recipient->address }} <i class="glyphicon glyphicon-map-marker">
                        </i></cite></small>
                        <p>
                            <i class="glyphicon glyphicon-envelope"></i>{{ $recipient->more_address }}
                                          
                            <br />
                            <i class="glyphicon glyphicon-gift"></i>{{ $recipient->city }}</p>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
