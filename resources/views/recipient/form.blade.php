<div class="form-group">
        
		{!! Form::label('recipient_name', 'Recipient Name:') !!}
		
		{!! Form::text('recipient_name', null, ['class' => 'form-control']) !!}
	
</div> <!-- /form-group -->

<!-- /body text value -->
<div class="form-group">
        
		{!! Form::label('country', 'Contry:') !!}
		
		{!! Form::text('country', null, ['class' => 'form-control']) !!}
	
</div> <!-- /form-group -->

<!-- published at value -->
<div class="form-group">
        
	{!! Form::label('address', 'Address:') !!}
	
	{!! Form::text('address', null, ['class' => 'form-control']) !!}
	
</div> <!-- /form-group -->

<!-- tags selection -->
<div class="form-group">
        
	{!! Form::label('more_address', 'More Address:') !!}
	
	{!! Form::text('more_address', null, ['class' => 'form-control']) !!}
	
</div> <!-- /form-group -->

<!-- /body text value -->
<div class="form-group">
        
	{!! Form::label('city', 'City:') !!}
	
	{!! Form::text('city', null, ['class' => 'form-control']) !!}
	
</div> <!-- /form-group -->

<!-- /body text value -->
<div class="form-group">
        
		{!! Form::label('state', 'State:') !!}
		
		{!! Form::text('state',null, ['class' => 'form-control']) !!}
	
</div> <!-- /form-group -->


<!-- /body text value -->
<div class="form-group">
        
		{!! Form::label('zip', 'Zip:') !!}
		
		{!! Form::text('zip', null,['class' => 'form-control']) !!}
	
</div> <!-- /form-group -->

<div class="form-group">
    {!! Form::label('Shipment Image') !!}
    {!! Form::file('image', null) !!}
</div>

<!-- /submit button value -->
<div class="form-group">
        
		{!! Form::submit($submitButtonText ,['class' => 'btn btn-primary form-control'] ) !!}
	
</div> <!-- /form-group -->