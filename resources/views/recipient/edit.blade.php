@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Edit Shipment</div>
				<div class="panel-body">
					
					<h1> Edit: {!! $recipient->recipient_name !!} </h1>

					<br/>
	
					@include('errors.list')
	
					<br/>	
					{!! Form::model($recipient, ['method'=> 'PATCH', 'action' => ['Recipient\RecipientController@update', $recipient->id] , 'files' => true])  !!}
	
						@include('recipient.form', ['submitButtonText' => 'Update Shipment'])
	
					{!! Form::close()  !!}
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
