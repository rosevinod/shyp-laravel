@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Add Shipment</div>
				<div class="panel-body">
					
					
					<br/>
	
					@include('errors.list')
	
					<br/>
					{!! Form::model($recipient = new \App\Recipient, ['url' => 'recipients', 'files' => true])  !!}
	
						@include('recipient.form', ['submitButtonText' => 'Add Shipment'])
	
					{!! Form::close()  !!}
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
